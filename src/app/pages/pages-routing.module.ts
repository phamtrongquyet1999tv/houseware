import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PagesComponent } from './pages.component';
import {HeaderComponent} from "./components/header/header.component";

const routes: Routes = [
    {
    path: '',
    component: PagesComponent,
    children: [
      {
        path: 'header',
        component: HeaderComponent
      }
    ]
    },
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
