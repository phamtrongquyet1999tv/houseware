import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  categories: any = ['All Categories', 'Milks and Dairies', 'Wines & Alcohol', 'Clothing & Beauty', 'Pet Foods & Toy', 'Fast food', 'Baking material', 'Vegetables', 'Fresh Seafood', 'Noodles & Rice', 'Ice cream'];
  categoryChoosen = this.categories[0];
  search: string = '';
  filteredCategories = this.categories;
  isCategoryVisible: boolean = false;

  constructor() {
  }

  ngOnInit(): void {
  }

  pickCategory(category: string) {
    this.categoryChoosen = category;
    this.isCategoryVisible = false;
  }

  searchCategory() {
    console.log(this.search)
    this.filteredCategories = this.categories.filter(
      (item: string) => this.search.trim() == '' ? true : item.includes(this.search)
    );
    this.filteredCategories = this.filteredCategories.length == 0 ? ['No results found'] : this.filteredCategories;
  }

  toggleCategoryDropdown() {
    this.isCategoryVisible = !this.isCategoryVisible;
  }
}
